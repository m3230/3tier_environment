resource "aws_alb" "web_nlb" {

    name                = "web-application-load-balancer"
    internal            = false
    load_balancer_type  = "application"
    security_groups     = [var.public_subnet_A, var.public_subnet_B] 
    subnets             = [var.alb_external_sg] 
    enable_deletion_protection = true

    tags = {

        Environment = "test"
    }
    
}

resource "aws_lb_target_group" "tg_instances" {

        name             = "Instances-tg"
        port             = 80
        protocol         = "HTTP"
        vpc_id           = var.vpc_id
        target_type      = "IP"
        
}

resource "aws_lb_listener" "web_external_listener" {

    load_balancer_arn     = aws_alb.web_nlb.arn
    port                  = 80
    protocol              = "HTTP"

    default_action {
      
      type       = "forward"
      target_group_arn = aws_lb_target_group.tg_instances.arn

    }


}