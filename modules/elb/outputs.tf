output "out_app_nlb" {
    value = aws_lb.app_nlb.arn
}

output "out_asg_tg" {
    value = aws_lb_target_group.asg_tg.arn
}

output "out_web_nlb" {
    value = aws_alb.web_nlb.arn
}

output "out_tg_instances" {
    value = aws_lb_target_group.tg_instances.arn
}

