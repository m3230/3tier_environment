resource "aws_lb" "app_nlb" {

    name                 = "Application-NLB"
    internal             = false  
    load_balancer_type   = "test"
    subnets              = [var.alb_internal_sg]
    security_groups      = [var.private_subnet_A, var.private_subnet_B] 
    enable_deletion_protection = true

    tags = {

      Environment = "test"

    }

}

resource "aws_lb_target_group" "asg_tg" {

    name       = "internal-target-group"
    port       = 3000
    protocol   = "TCP"
    vpc_id     = var.vpc_id 
    target_type = "ip"  

}

resource "aws_lb_listener" "app_internal_listener" {

    load_balancer_arn = aws_lb.app_nlb.arn
    port              = 3000
    protocol          = "TCP"

    default_action {
      type = "forward"
      target_group_arn  = aws_lb_target_group.asg_tg.arn
      
    }

  
}
