#EC2 Host for private subnets
resource "aws_instance" "private_host_A" {

    ami                = var.ami
    instance_type      = var.instance_type
    subnet_id          = var.private_subnet_A
    key_name           = "mypemkey"
    vpc_security_group_ids = [var.bastion_host_sg, var.private_sg_nat]

    tags = {
        Name = "PrivateA instance"
    }
    

}

resource "aws_instance" "private_host_B" {

    ami                = var.ami
    instance_type      = var.instance_type
    subnet_id          = var.private_subnet_B
    key_name           = "mypemkey"
    vpc_security_group_ids = [var.bastion_host_sg, var.private_sg_nat]

    tags = {
        Name = "PrivateB instance"
    }
    

}

#Bastion Host
resource "aws_instance" "bastion_host" {

    ami                = var.ami
    instance_type      = var.instance_type
    subnet_id          = var.public_subnet_A
    associate_public_ip_address = true
    key_name           = "mypemkey"
    vpc_security_group_ids = [var.bastion_host_sg]


    tags = {
        Name = "Bastion_host"

    }
  
}

#Elastic IP
resource "aws_eip" "lb" {

    instance = aws.instance.bastion_host.id
    vpc      = true
  
}

