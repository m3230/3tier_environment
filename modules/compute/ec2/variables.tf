variable "ami" {

    default = "ami-02f5781cba46a5e8" #Amazon Linux

}

variable "instance_type" {

    default = "t2.micro"
  
}

variable "private_subnet_A" {}
variable "bastion_host_sg" {}
variable "private_sg_nat" {}
variable "private_subnet_B" {}
variable "public_subnet_A" {}