variable "max_size" {

    default = "3"

}

variable "min_size" {

    default = "2"
  
}

variable "app_launch_config" {}
variable "web_launch_config" {}
variable "out_tg_instances" {}
variable "asg_tg" {}
variable "public_subnet_A" {}
variable "public_subnet_B" {}
variable "private_subnet_A" {}
variable "private_subnet_B" {}
