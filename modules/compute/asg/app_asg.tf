resource "aws_autoscaling_group" "appasg" {

    name                  = "application_asg"
    max_size              = var.max_size
    min_size              = var.min_size
    launch_configuration  = var.app_launch_config
    target_group_arns     = [var.asg_tg]
    health_check_type     = "EC2"
    health_check_grace_period = 300
    force_delete          = true
    vpc_zone_identifier   = [var.private_subnet_A, var.private_subnet_B]
    
}