resource "aws_autoscaling_group" "webasg" {

    name                 = "web_asg"
    max_size             = var.max_size
    min_size             = var.min_size
    launch_configuration = var.web_launch_config
    target_group_arns    = [var.out_tg_instances]
    health_check_grace_period = 300
    health_check_type    = "EC2"
    force_delete         = true
    vpc_zone_identifier  = [var.public_subnet_A, var.public_subnet_B]   

}