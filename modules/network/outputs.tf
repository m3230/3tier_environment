#vpc outputs
output "out_vpc_id" {
    value = aws_vpc.main.id
}

output "out_public_subnet_A" {
    value = aws_subnet.publicsubnetA.id    
}

output "out_public_subnet_B" {
    value = aws_subnet.publicsubnetB.id
}

output "out_private_subnet_A" {
    value = aws_subnet.privatesubnetA.id
}

output "out_private_subnet_B" {
    value = aws_subnet.privatesubnetB.id
}

output "out_db_subnet" {
    value = aws_db_subnet_group.default.id
}

#internet_gateway
output "out_igw" {
    value = aws_internet_gateway.gw.id
}

#NAT
output "out_nat_gw" {
    value = aws_nat_gateway.natgw.id
}

#Private Subnet SG
output "out_private_sg_nat" {
    value = aws_security_group.private_sg_nat.id
}

#Public Subnet SG
output "out_public_sg_nat" {
    value = aws_security_group.public_sg_nat.id
}

#Bastion Host
output "out_bastion_host_sg" {
    value = aws_security_group.bastion_host_sg.id
}

#SSH Bastion Host
output "out_bastion_ssh_via_NAT" {
    value = aws_security_group.bastion_ssh_via_NAT.id
}

#External ALB
output "out_alb_external_sg" {
    value = aws_security_group.alb_external_sg.id
}

#Internal ALB
output "out_alb_internal_sg" {
    value = aws_security_group.alb_internal_sg.id  
}

#Launch Config Access
output "out_launch_web_sg" {
    value = aws_security_group.launch_web_sg.id  
}

#Database SG
output "out_database_sg" {
    value = aws_security_group.database_sg.id  
}