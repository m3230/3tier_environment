resource "aws_vpc" "main" {

    cidr_block = var.vpc_cidr_block

}

resource "aws_subnet" "publicsubnetA" {

    availability_zone = var.availability_zone_A
    vpc_id = aws_vpc.main.id
    cidr_block = var.public_subnet_A
    
}

resource "aws_subnet" "publicsubnetB" {

    availability_zone = var.availability_zone_B
    vpc_id = aws_vpc.main.id
    cidr_block = var.public_subnet_B

}

resource "aws_subnet" "privatesubnetA" {

    availability_zone = var.availability_zone_A
    vpc_id = aws_vpc.main.id
    cidr_block = var.private_subnet_A

}

resource "aws_subnet" "privatesubnetB" {

    availability_zone = var.availability_zone_B
    vpc_id = aws_vpc.main.id
    cidr_block = var.private_subnet_B

}

#Defining RDS Subnet Group
resource "aws_db_subnet_group" "default" {

    name = "main"
    subnet_ids = [aws_subnet.privatesubnetA.id, aws_subnet.privatesubnetB.id]

    tag = {
        name = "my DB subnet group"
        
    }
    
}
