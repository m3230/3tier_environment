#public_route_table
resource "aws_route_table" "public_rt" {
    vpc_id = aws_vpc.main.id

    route = [
        {
            cidr_block = var.allow_any
            gateway_id = aws_internet_gateway.gw.id

        }

    ]

    tags = {
        Name = "Public RT"
    }

}

resource "aws_route_table_association" "publicA" {
    subnet_id = aws_subnet.publicsubnetA.id
    route_table_id = aws_route_table.public_rt.id
}

resource "aws_route_table_association" "publicB" {
    subnet_id = aws_subnet.publicsubnetB.id
    route_table_id = aws_route_table.public_rt.id
}

#private_route_table
resource "aws_route_table" "private_rt" {
    vpc_id = aws_vpc.main.id

    route = [
        {
            cidr_block = var.allow_any
            gateway_id = aws_internet_gateway.gw.id

        }
    ]
    
    tags = {
      Name = "Private RT"

    }

}

resource "aws_route_table_association" "privateA" {
    subnet_id = aws_subnet.privatesubnetA.id
    route_table_id = aws_route_table.private_rt.id 
}

resource "aws_route_table_association" "privateB" {
    subnet_id = aws_subnet.privatesubnetB.id
    route_table_id = aws_route_table.private_rt.id  
}