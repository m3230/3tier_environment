resource "aws_eip" "nat_eip" {

    vpc = true

}

resource "aws_nat_gateway" "natgw" {

    allocation_id = aws_eip.nat_eip.id
    subnet_id = aws_subnet.publicsubnetB.id

    tags = {

        Name = "gw NAT"

    }

    depends_on = [aws_internet_gateway.gw]

}