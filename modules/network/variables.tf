variable "vpc_cidr_block" {

    default = "10.0.0.0/16"

}

variable "public_subnet_A" {

    default = "10.0.1.0/24"

}

variable "public_subnet_B" {

    default = "10.0.2.0/24"

}

variable "private_subnet_A" {

    default = "10.0.3.0/24"

}

variable "private_subnet_B" {

    default = "10.0.4.0/24"

}

variable "allow_any" {

    default = "0.0.0.0/0"

}

variable "availability_zone_A" {

    default = "eu-west-2a" 

}

variable "availability_zone_B" {

    default = "eu-west-2b"
    
}