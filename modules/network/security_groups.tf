#SG for Web Access thru NAT
#Private Subnet SG
resource "aws_security_group" "private_sg_nat" {

    name = "allow_private_subnet"
    description = "Allow access to NAT instances through private subnet"
    vpc_id = aws_vpc.main.id

    ingress {
        from_port = 80
        to_port   = 80
        protocol  = "tcp"
        cidr_blocks = [aws_subnet.privatesubnetA.cidr_block]
    }

    ingress {
        from_port = 443
        to_port   = 443
        protocol = "tcp"
        cidr_blocks = [aws_subnet.privatesubnetA.cidr_block]
    }

    ingress {
        from_port = -1
        to_port   = -1
        protocol = -"icmp"
        cidr_blocks = [aws_subnet.privatesubnetA.cidr_block]
    }

    ingress {
        from_port = 80
        to_port   = 80
        protocol  = "tcp"
        cidr_blocks = [aws_subnet.privatesubnetB.cidr_block]
    }

    ingress {
        from_port = 443
        to_port   = 443
        protocol = "tcp"
        cidr_blocks = [aws_subnet.privatesubnetB.cidr_block]
    }

    ingress {
        from_port = -1
        to_port   = -1
        protocol = "icmp"
        cidr_blocks = [aws_subnet.privatesubnetB.cidr_block]
    }

    egress {
        from_port = 0
        to_port   = 0
        protocol  = -1
        cidr_blocks =  [var.allow_any]
    }

    tags = {
        Name =  "Access to NAT instance through private sg"
        
    }

}

#Public Subnet SG

resource "aws_security_group" "public_sg_nat" {

    name = "allow_public_subnet"
    description = "Allow access to private subnet thru NAT fom public subnet"
    vpc_id = aws_vpc.main.id

    ingress {
        from_port = 80
        to_port   = 80
        protocol  = "tcp"
        cidr_blocks = [aws_subnet.publicsubnetA.cidr_block]
    }

    ingress {
        from_port = 443
        to_port   = 443
        protocol = "tcp"
        cidr_blocks = [aws_subnet.publicsubnetA.cidr_block]
    }

    ingress {
        from_port = -1
        to_port   = -1
        protocol = -"icmp"
        cidr_blocks = [aws_subnet.publicsubnetA.cidr_block]
    }

    ingress {
        from_port = 80
        to_port   = 80
        protocol  = "tcp"
        cidr_blocks = [aws_subnet.publicsubnetB.cidr_block]
    }

    ingress {
        from_port = 443
        to_port   = 443
        protocol = "tcp"
        cidr_blocks = [aws_subnet.publicsubnetB.cidr_block]
    }

    ingress {
        from_port = -1
        to_port   = -1
        protocol = "icmp"
        cidr_blocks = [aws_subnet.publicsubnetB.cidr_block]
    }

    egress {
        from_port = 0
        to_port   = 0
        protocol  = -1
        cidr_blocks =  [var.allow_any]
    }

    tags = {
        Name =  "Access to private subnet thru NAT"
        
    }
  
}

#SG for Bastion Host

resource "aws_security_group" "bastion_host_sg" {

    name = "bastion_host_sg"
    description = "Allow access to bastion host"
    vpc_id = aws_vpc.main.id

    ingress {
        from_port = 22
        to_port   = 22
        protocol = "tcp"
        cidr_blocks = [var.allow_any] 
    }

    egress {
        from_port = 0
        to_port   = 0
        protocol = -1
        cidr_blocks = [var.allow_any]
    }

    tags = {
        Name = "Bastion host access"
    }
  
}

#SSH from Bastion

resource "aws_security_group" "bastion_ssh_via_NAT" {
    name = "bastion_ssh_via_NAT"
    description = "Allow SSH from Bastion Host"
    vpc_id = aws_vpc.main.id

    ingress {
        from_port = 22
        to_port   = 22
        protocol  = "tcp"
        security_groups = [
            aws_security_group.bastion_host_sg.id,
            aws_security_group.private_sg_nat.id

        ]
    }
}

#External Application Load Balancer

resource "aws_security_group" "alb_external_sg" {

    name = "alb_external-sg"
    description = "Allow TCP protocol"
    vpc_id = aws_vpc.main.id

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = [var.allow_any]

    }

    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = [var.allow_any]

    }

    tags = {
        Name = "External ALB SG"

    }
  
}

#Internal Application Load Balancer

resource "aws_security_group" "alb_internal_sg" {

    name = "alb_internal_sg"
    description = "Allow TCP protocol"
    vpc_id = aws_vpc.main.id

    ingress {
        from_port = 3000 #without root access
        to_port = 3000
        protocol = "tcp"
        cidr_blocks = [var.private_subnet_A]

    }

    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = [var.private_subnet_A]
    }

    ingress {
        from_port = 3000
        to_port = 3000
        protocol = "tcp"
        cidr_blocks = [var.private_subnet_B]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = [var.private_subnet_B]
    }

    tags = {
      Name = "Internal ALB SG"

    }
  
}

#Launch configuration SG

resource "aws_security_group" "launch_web_sg" {

    name = "launch_web_sg"
    description = "Allow access to launch config"
    vpc_id = aws_vpc.main.id

    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = [var.allow_any]
    }

}

#Database SG

resource "aws_security_group" "database_sg" {

    name = "database_sg"
    description = "Allow access to RDS"
    vpc_id = aws_vpc.main.id

    ingress {
        from_port = 3306 #Default MySQL port
        to_port = 3306
        protocol = "tcp"
        cidr_blocks = [aws_subnet.privatesubnetA.cidr_block]    
    }

    ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = [aws_subnet.privatesubnetA.cidr_block]        
    }

    ingress {
        from_port = 3306 #Default MySQL port
        to_port = 3306
        protocol = "tcp"
        cidr_blocks = [aws_subnet.privatesubnetB.cidr_block]    
    }

    ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = [aws_subnet.privatesubnetB.cidr_block]        
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = [var.allow_any]

    }

    tags = {
      Name = "Database SG"

    }
  
}