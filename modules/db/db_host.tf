resource "aws_db_instance" "rds_db" {

    allocated_storage            = 30
    engine                       = "mysql"
    engine_version               = "5.7"
    instance_class               = "db.t3.micro"
    name                         = "mydb"
    username                     = "admin"
    password                     = "abcd123"
    storage_type                 = "gp2"
    multi_az                     = true
    port                         = 3306
    db_subnet_group_name         = var.database_sg
    vpc_security_group_ids       = [var.out_database_sg]

    tags = {
        name   = "MySQL DB"
    }


}