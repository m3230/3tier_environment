output "out_app_config" {

    value = aws_launch_configuration.app_conf.name
}

output "out_web_config" {

    value   = aws_launch_configuration.web_config.name
  
}