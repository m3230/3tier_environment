resource "aws_launch_configuration" "app_conf" {

    name                      = "Application-launch-configuration"
    image_id                  = var.ami 
    instance_type             = var.instance_type_max
    security_groups           = [var.out_alb_internal_sg, var.out_bastion_host_sg]
    key_name                  = "mypemkey" 


}
