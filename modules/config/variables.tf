variable "ami" {

    default = "ami-02f5781cba46a5e8" #Amazon Linux

}

variable "instance_type" {

    default = "t2.micro"
  
}

variable "instance_type_max" {

    default = "t2.medium"
  
}

variable "out_bastion_host_sg" {}
variable "out_alb_internal_sg" {}
variable "out_alb_external_sg" {}
variable "out_launch_web_sg" {}
