resource "aws_launch_configuration" "web_config" {

    name                    = "Web-launch-configuration"
    image_id                = var.ami 
    instance_type           = var.instance_type
    security_groups         = [var.out_alb_external_sg, var.out_bastion_host_sg, var.out_launch_web_sg]
    user_data               = file("./config/userdata.sh")
    key_name                = "mypemkey" 
    associate_public_ip_address = true 
}