module "vpc" {
    
    source = "./network/vpc"
    
}

module "ec2" {

    source = "./compute/ec2"
    
    public_subnet_A = module.vpc.out_public_subnet_A
    private_subnet_A = module.vpc.out_private_subnet_A
    private_subnet_B = module.vpc.out_private_subnet_B
    bastion_host_sg  = module.vpc.out_bastion_host_sg
    private_sg_nat   = module.vpc.out_private_sg_nat

}

module "elb" {

    source = "./elb"

    public_subnet_A = module.vpc.out_public_subnet_A
    public_subnet_B = module.vpc.out_public_subnet_B
    private_subnet_A = module.vpc.out_private_subnet_A
    private_subnet_B = module.vpc.out_private_subnet_B
    alb_external_sg  = module.vpc.out_alb_external_sg
    alb_internal_sg  = module.vpc.out_alb_internal_sg
    vpc_id           = module.vpc.out_vpc_id

}

module "asg" {

    source = "./compute/asg"

    public_subnet_A = module.vpc.out_public_subnet_A
    public_subnet_B = module.vpc.out_public_subnet_B
    private_subnet_A = module.vpc.out_private_subnet_A
    private_subnet_B = module.vpc.out_private_subnet_B
    app_config       = module.config.out_app_config
    web_config       = module.config.out_web_config
    tg_instances     = module.elb.out_tg_instances
    asg_tg           = module.elb.out_asg_tg


}

module "db" {

    source = "./db"

    private_subnet_A    = module.vpc.out_private_subnet_A
    private_subnet_B    = module.vpc.out_private_subnet_B
    database_sg         = module.vpc.out_database_sg
    out_database_sg     = module.vpc.out_database_sg

}

module "config" {

    source = "./config"

    out_launch_web_sg       = module.vpc.out_launch_web_sg
    out_alb_external_sg     = module.vpc.out_alb_external_sg
    out_alb_internal_sg     = module.vpc.out_alb_internal_sg
    out_bastion_host_sg     = module.vpc.out_bastion_host_sg

}