# **3tier_environment**


## **Cloud Provider - AWS**
### **IaC tool used - Terraform**

#### **Resources created :**

- EC2 instances
- Auto Scaling group
- Bastion Host
- RDS
- ELB (Internet and the Internal Load balancer)
- VPC
- Internet gateway
- 4 Subnets (2 public and 2 private in 2 AZ's)
- Create two Route tables (public for internet and private for the traffic through NAT Gateway)
- Create NAT Gateway


